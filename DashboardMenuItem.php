<?php

namespace App\Models;

class MenuItem extends Model
{
    use AdminBuilder, ModelTree;
    use HasFactory;
    use HasTranslations;
    use Sluggable;

    public const TYPE_EXTERNAL_LINK = 'external_link';

    public const TYPE_INTERNAL_LINK = 'internal_link';

    public $translatable = [
        'name',
    ];

    protected $guarded = [];

    protected $casts = [
        'page_id' => Uuid::class,
    ];

    public function __construct($attributes = [])
    {
        parent::__construct($attributes);

        $this->setParentColumn('parent_id');
        $this->setOrderColumn('lft');
        $this->setTitleColumn('name');
        parent::addGlobalScope(new OrderByScope('lft'));
    }

    public static function getTree()
    {
        if (user()) {
            $abilities = user()->employeeAbilities();

            if (user()->isEmployeeAdministrator()) {
                return self::getAllTree();
            }
        } else {
            $abilities = null;
        }

        $menu = self::orderBy('lft')
            ->with('parent')
            ->get();

        if ($menu->count()) {
            foreach ($menu as $k => $menuItem) {
                $menuItem->children = collect([]);

                if ($abilities && empty($abilities[$menuItem->slug]) && $menuItem->slug !== 'homepage' && $menuItem->slug !== 'chat') {
                    $menu = $menu->reject(function ($item) use ($menuItem) {
                        return $item->id === $menuItem->id;
                    });

                    continue;
                }

                foreach ($menu as $i => $menuSubitem) {
                    if ($menuSubitem->parent_id === $menuItem->id) {
                        if ($abilities && empty($abilities[$menuItem->slug])) {
                            $menu = $menu->reject(function ($item) use ($menuItem) {
                                return $item->id === $menuItem->id;
                            });

                            continue 2;
                        }

                        $menuItem->children->push($menuSubitem);
                        $menu = $menu->reject(function ($item) use ($menuSubitem) {
                            return $item->id === $menuSubitem->id;
                        });
                    }
                }
            }
        }

        return $menu;
    }

    public static function getAllTree()
    {
        $menu = self::orderBy('lft')->get();

        if ($menu->count()) {
            foreach ($menu as $k => $menuItem) {
                $menuItem->children = collect([]);

                foreach ($menu as $i => $menuSubitem) {
                    if ($menuSubitem->parent_id === $menuItem->id) {
                        $menuItem->children->push($menuSubitem);
                        $menu = $menu->reject(function ($item) use ($menuSubitem) {
                            return $item->id === $menuSubitem->id;
                        });
                    }
                }
            }
        }

        return $menu;
    }

    public function url()
    {
        switch ($this->type) {
            case 'external_link':
                return $this->link;
            case 'internal_link':
                return url($this->link);
            default:
                return url($this->page->slug);
        }
    }
}
