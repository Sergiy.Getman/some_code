<?php

namespace App\Http\Controllers;

class MessageController extends Controller
{
    /**
     * Show all the message threads to the user.
     */
    public function index(): View
    {
        $threads = Thread::forUser(auth()->id())
            ->where('offer_id', null)
            ->with(['participants', 'users', 'messages', 'participants.user'])
            ->latest('updated_at')
            ->get();

        SEO::setTitle(trans('messenger.messenger').' - '.settings('page_name').' » '.settings('sub_title'));

        if ($threads->isEmpty()) {
            return view('frontend.messenger.no-threads');
        }
        return view('frontend.messenger.index', ['threads' => $threads]);
    }

    /**
     * Shows a message thread.
     */
    public function show(int $id): RedirectResponse|View
    {
        if (! request()->ajax()) {
            return redirect()->route('messages');
        }

        try {
            $thread = Thread::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return redirect()->route('messages');
        }

        $userId = auth()->id();
        $participant = $thread->participants->where('user_id', $userId)->first();
        if (! isset($participant)) {
            return redirect()->route('messages');
        }

        $messages = $thread->messages()->latest()->paginate(32);
        $thread->markAsRead($userId);
        return view('frontend.messenger.show', ['thread' => $thread, 'messages' => $messages]);
    }

    /**
     * Check for new messages in the thread.
     */
    public function check(int $id): RedirectResponse|int
    {
        try {
            $thread = Thread::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            session()->flash('error_message', 'The thread with ID: '.$id.' was not found.');
            return redirect()->route('messages');
        }

        return $thread->userUnreadMessagesCount(auth()->id());
    }

    /**
     * Stores a new message thread.
     *
     * @return mixed
     */
    public function store(Request $request): RedirectResponse
    {
        $input = $request->all();

        if (strlen(trim($input['message'])) === 0) {
            Alert::error('<i class="fa fa-times m-r-5"></i>'.trans('messenger.alert.no_input'))->flash();
            return back();
        }

        if (auth()->id() === $input['recipient']) {
            Alert::error('<i class="fa fa-times m-r-5"></i>'.trans('messenger.alert.self_message'))->flash();
            return back();
        }

        $recipient = User::find($input['recipient']);
        if (! isset($recipient)) {
            Alert::error('<i class="fa fa-times m-r-5"></i>'.trans('messenger.alert.unkown_recipient'))->flash();
            return back();
        }

        $thread = Thread::between([auth()->id(), $input['recipient']])->where('offer_id', null)->first();

        if (! isset($thread)) {
            $thread = Thread::create([
                'subject' => 'messenger',
            ]);

            Participant::create([
                'thread_id' => $thread->id,
                'user_id' => auth()->id(),
                'last_read' => new Carbon(),
            ]);

            if ($request->has('recipient')) {
                $thread->addParticipant($input['recipient']);
            }
        } else {
            $latest_message = $thread->latest_message;
            if (isset($latest_message) && $latest_message->created_at->addSeconds(10) > now() && $latest_message->body === $request->input('message')) {
                Alert::error('<i class="fa fa-times m-r-5"></i>'.trans('messenger.alert.duplicate_message'))->flash();

                return redirect()->route('messages');
            }
        }

        Message::create([
            'thread_id' => $thread->id,
            'user_id' => auth()->id(),
            'body' => $input['message'],
        ]);

        $receiver_part = $thread->participants->where('user_id', '!=', auth()->id())->first();

        $receiver = User::find($receiver_part->user_id);

        $check_array = [
            'thread_id' => $thread->id,
            'user_id' => auth()->user()->id,
        ];

        $notification_check = $receiver->notifications()->where('data', json_encode($check_array))->first();

        return redirect()->route('messages');
    }

    /**
     * Adds a new message to a current thread.
     */
    public function update(Request $request, int $id): RedirectResponse|int
    {
        if (! request()->ajax()) {
            return redirect()->route('messages');
        }

        try {
            $thread = Thread::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            session()->flash('error_message', 'The thread with ID: '.$id.' was not found.');

            return redirect()->route('messages');
        }
        $thread->activateAllParticipants();

        if (strlen(trim($request->input('message'))) === 0) {
            abort(406, trans('messenger.alert.no_input'));
        }

        $participant = $thread->participants->where('user_id', auth()->id())->first();
        if (! isset($participant)) {
            return redirect()->route('messages');
        }

        $latest_message = $thread->latest_message;
        if (isset($latest_message) && $latest_message->created_at->addSeconds(10) > now() && $latest_message->body === $request->input('message')) {
            abort(429, trans('messenger.alert.duplicate_message'));
        }

        Message::create([
            'thread_id' => $thread->id,
            'user_id' => auth()->id(),
            'body' => $request->input('message'),
        ]);

        $participant = Participant::firstOrCreate([
            'thread_id' => $thread->id,
            'user_id' => auth()->id(),
        ]);
        $participant->last_read = new Carbon();
        $participant->save();

        $receiver_part = $thread->participants->where('user_id', '!=', auth()->id())->first();

        $receiver = User::find($receiver_part->user_id);

        $check_array = [
            'thread_id' => $thread->id,
            'user_id' => auth()->user()->id,
        ];

        $notification_check = $receiver->notifications()->where('data', json_encode($check_array))->first();

        return $id;
    }
}
